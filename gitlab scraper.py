# Imports

from bs4 import BeautifulSoup
import requests
import csv

#Pulling http data from url
source = requests.get('https://gitlab.com/users/demarb/projects').text

#sending that data through bs4 and parsing html with lxml
soup = BeautifulSoup(source, 'lxml')

print(soup.prettify())